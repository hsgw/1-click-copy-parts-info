var PARTS_DATA_ITEM = ["MfgPartNum","MfgName","VID","VendorPartNum","Description","PkgCode"];

function onClickMenu(info,tab){
	chrome.tabs.sendMessage(
		tab.id, 
		{text: 'report'}, 
		function(data){
			var tempStr = "";

			for(i in PARTS_DATA_ITEM){
				if(data[PARTS_DATA_ITEM[i]] !== undefined){
					tempStr += data[PARTS_DATA_ITEM[i]];
				}
				tempStr += "\t";
			}

			tempStr += info.pageUrl;

			var textArea = document.createElement("textarea");
			textArea.style.cssText = "position:absolute;left:-100%";
			document.body.appendChild(textArea);
			textArea.value = tempStr;
			textArea.select();
			document.execCommand("copy");
			document.body.removeChild(textArea);
		}
	);
}

chrome.contextMenus.create({
	"title" : "Copy Parts Info",
	"documentUrlPatterns" : ["http://www.mouser.jp/ProductDetail/*","http://akizukidenshi.com/catalog/g/*"],
	"onclick": onClickMenu
});