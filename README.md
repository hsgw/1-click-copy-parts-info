1 Click copy parts info
=================================
mouser.jpや秋月の商品ページから1クリックでパーツの情報をコピーするchrome拡張です。
タブ区切りでクリップボードへ格納されているので直接Excelなど表計算ソフトへペーストできます。


使い方
---------------------------------
mouser.jpや秋月の商品ページでコンテキストメニューを開いて「Copy parts info」をクリックし、表計算ソフトへペーストします。


eagleのBOM-EX用パーツデータベースのフォーマットとして以下の通りにタブ区切りでコピーされます。

|manufacture part number|manufacturer name|vendor ID|vendor part number|description| package name|URL|


注意
---------------------------------
mouser**.jp**にしか対応していません。


ライセンス
---------------------------------
Copyright (c) 2015 Takuya Urakawa(dm9records.com)

Released under the MIT license
http://opensource.org/licenses/mit-license.php


なにかあったら
---------------------------------
[@hsgw](http://twitter.com/hsgw)まで