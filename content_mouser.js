function mouserParser (msg, sender, sendResponse) {
	if (msg.text === 'report') {
		var data = {
			"MfgPartNum": document.getElementById("divManufacturerPartNum").innerText.trim(),
			"MfgName": document.getElementById("ctl00_ContentMain_hlnk10").innerText.trim(),
			"VID": "ME",
			"VendorPartNum": document.getElementById("divMouserPartNum").innerText.trim(),
			"Description": document.getElementById("divDes").innerText.trim(),
			"PkgCode": undefined,
		};

		var temp = document.getElementById("spec").getElementsByTagName("table")[0].innerText.split(/\r\n|\r|\n|:/);

		for(var i=0;i<temp.length;i+=2){
			if(temp[i].indexOf("パッケージ/ケース") !== -1 || temp[i].indexOf("パッケージ/ケース") !== -1) {
				data["PkgCode"] = temp[i+1].trim();
				break;
			}
		}
		sendResponse(data);
	}
}

chrome.runtime.onMessage.addListener(mouserParser);
