String.prototype.toHalfWidth = function() {
	return this.replace(/[！-～]/g, function(s) {
		return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
	}).replace(/　/g," ");
}

function akizukiParser (msg, sender, sendResponse) {
	if (msg.text === 'report') {
		var raw = document.getElementsByClassName("cart_table")[0].children[0].children[0].getElementsByTagName("table")[3].getElementsByTagName("td");

		var tempNode = raw[0].cloneNode(true);

		try{
			tempNode.removeChild(tempNode.getElementsByTagName("h6")[0]);
			if(tempNode.getElementsByTagName("img").length > 0) {
				tempNode.removeChild(tempNode.getElementsByTagName("img")[0]);
			}
		} catch (e) {
			console.log(e);
		}

		var tempStr = tempNode.innerHTML.split("<br>");

		for(var i=0;i<tempStr.length;i++) {
			tempStr[i] = tempStr[i].replace(/^\s+|\s+$|\r?\n/g, "");
		}

		var data = {
			"MfgPartNum": tempStr[0].substr(1, tempStr[0].length - 2),
			"MfgName": raw[0].getElementsByTagName("span")[0].getElementsByTagName("a")[0].innerText,
			"VID": "AKI",
			"VendorPartNum": tempStr[1].substr(6, tempStr[1].length - 2),
			"Description": raw[0].getElementsByTagName("h6")[0].innerText.toHalfWidth(),
			"PkgCode": undefined,
		};

		sendResponse(data);
	}
}

chrome.runtime.onMessage.addListener(akizukiParser);
